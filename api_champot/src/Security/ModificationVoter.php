<?php

namespace App\Security;


use App\Entity\Modification;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ModificationVoter extends Voter {

  const SHOW = 'show';

  const EDIT = 'edit';

  /**
   * @var AccessDecisionManagerInterface
   */
  private $decisionManager;

  /**
   * @var Security
   */
  private $security;

  public function __construct(AccessDecisionManagerInterface $decisionManager,Security $security) {

    $this->decisionManager = $decisionManager;
    $this->security = $security;
  }


  /**
   * Determines if the attribute and subject are supported by this voter.
   *
   * @param string $attribute An attribute
   * @param mixed $subject The subject to secure, e.g. an object the user wants
   *   to access or any other PHP type
   *
   * @return bool True if the attribute and subject are supported, false
   *   otherwise
   */
  protected function supports($attribute, $subject) {

    if (!in_array($attribute, [self::SHOW, self::EDIT])) {

      return FALSE;
    }

    if (!$subject instanceof Modification) {

      return FALSE;
    }

    return TRUE;
  }

  /**
   * Perform a single access check operation on a given attribute, subject and
   * token. It is safe to assume that $attribute and $subject already passed
   * the "supports()" method check.
   *
   * @param string $attribute
   * @param mixed $subject
   * @param TokenInterface $token
   *
   * @return bool
   */
  protected function voteOnAttribute($attribute, $subject, TokenInterface $token) {

    if ($this->security->isGranted('ROLE_ADMIN')) {

      return TRUE;
    }

    $user = $token->getUser();

    switch ($attribute) {

      case self::SHOW:
        return in_array('ROLE_ADMIN', $token->getRoleNames()) || in_array('ROLE_SUPER_ADMIN', $token->getRoleNames()) || $this->isUserHimself($subject, $token);

      case self::EDIT:
        return in_array('ROLE_ADMIN', $token->getRoleNames()) || in_array('ROLE_SUPER_ADMIN', $token->getRoleNames()) || $this->isUserHimself($subject, $token);
    }

    return FALSE;
  }

  /**
   * @param $subject
   * @param TokenInterface $token
   *
   * @return bool
   */
  protected function isUserHimself($subject, TokenInterface $token): bool {

    $authenticatedUser = $token->getUser();

    if (!$authenticatedUser instanceof User) {
      
      return FALSE;
    }

    /** @var User $user */
    $user = $subject;

    return $authenticatedUser->getId() === $user->getId();
  }
}
