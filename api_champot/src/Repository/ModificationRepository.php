<?php

namespace App\Repository;

use App\Entity\Champignon;
use App\Entity\Modification;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Modification|null find($id, $lockMode = null, $lockVersion = null)
 * @method Modification|null findOneBy(array $criteria, array $orderBy = null)
 * @method Modification[]    findAll()
 * @method Modification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Modification::class);
    }

    public function countNumberOfImages(User $user)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.author = :author')
            ->setParameter('author', $user)
            ->andWhere('m.image IS NOT NULL')
            ->select('count(m.id) as numberOfImages')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function countNumberOfModifications(User $user)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.author = :author')
            ->setParameter('author', $user)
            ->andWhere('m.champignon IS NOT NULL')
            ->select('count(m.id) as numberOfModifications')
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Modification[] Returns an array of Modification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Modification
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
