<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Repository\PropositionRepository;
use App\Entity\EntityTrait\SoftDeleteTrait;
use App\Entity\EntityTrait\CreatedAtTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=PropositionRepository::class)
 */
class Proposition
{

    use SoftDeleteTrait;
    use CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Modification::class, inversedBy="propositions")
     */
    private $modification;

    /**
     * @ORM\ManyToOne(targetEntity=Champignon::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $champignon;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="propositions")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="propositions_acceptations"),
     * @ORM\JoinTable(name="proposition_user_acceptations")
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $acceptations;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="propositions_refus"),
     * @ORM\JoinTable(name="proposition_user_refus")
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $refus;

    public function __construct()
    {
        $this->acceptations = new ArrayCollection();
        $this->refus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModification(): ?Modification
    {
        return $this->modification;
    }

    public function setModification(?Modification $modification): self
    {
        $this->modification = $modification;

        return $this;
    }

    public function getChampignon(): ?Champignon
    {
        return $this->champignon;
    }

    public function setChampignon(?Champignon $champignon): self
    {
        $this->champignon = $champignon;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAcceptations(): Collection
    {
        return $this->acceptations;
    }

    public function addAcceptation(User $acceptation): self
    {
        if (!$this->acceptations->contains($acceptation)) {
            $this->acceptations[] = $acceptation;
        }

        return $this;
    }

    public function removeAcceptation(User $acceptation): self
    {
        $this->acceptations->removeElement($acceptation);

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getRefus(): Collection
    {
        return $this->refus;
    }

    public function addRefu(User $refu): self
    {
        if (!$this->refus->contains($refu)) {
            $this->refus[] = $refu;
        }

        return $this;
    }

    public function removeRefu(User $refu): self
    {
        $this->refus->removeElement($refu);

        return $this;
    }
}
