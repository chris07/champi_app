<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\GetMeAction;
use App\Controller\LogoutAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\EntityTrait\SoftDeleteTrait;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use App\Entity\EntityTrait\CreatedAtTrait;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity("username")
 *
 * @ApiResource(
 *     attributes={"pagination_items_per_page"=30},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "post"
 *     },
 *     itemOperations={
 *         "get"={
 *             "requirements"={"id"="\d+"},
 *             "security"="is_granted('show', object)"
 *         },
 *         "patch"={"security"="is_granted('edit', object)"},
 *         "delete"={"security"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "get_me"={
 *             "method"="GET",
 *             "path"="/users/me",
 *             "controller"=GetMeAction::class,
 *             "read"=false
 *         },
 *          "logout"={
 *             "method"="GET",
 *             "path"="/users/logout",
 *             "controller"=LogoutAction::class,
 *             "openapi_context"={
 *                  "responses" = {
 *                      "200" = {
 *                          "description" = "user is logged out",
 *                          "schema" =  {
 *                              "type" = "object",
 *                              "properties" = {}
 *                          }
 *                      }
 *                  }
 *             },
 *             "read"=false
 *         }
 *     },
 *     normalizationContext={"groups"={"user:read"}, "enable_max_depth"=true},
 *     denormalizationContext={"groups"={"user:write"}}
 * )
 * @ApiFilter(DateFilter::class, properties={"createdAt", "deletedAt"})
 * @ApiFilter(SearchFilter::class, properties={"userName": "partial"})
 * @ApiFilter(BooleanFilter::class, properties={"banned"})
 * @ApiFilter(OrderFilter::class, properties={"createdAt", "deletedAt", "userName"}, arguments={"orderParameterName"="order"})
 */
class User implements UserInterface
{
    use SoftDeleteTrait;
    use CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("user:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:read", "user:write"})
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     * @Groups({"user:read", "user:write"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups("user:write")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity=Champignon::class, mappedBy="acceptations")
     * @Groups("user:read")
     * @ApiProperty(readableLink=false, writableLink=true)
     */
    private $champignons_acceptations;

    /**
     * @ORM\ManyToMany(targetEntity=Champignon::class, mappedBy="refus")
     * @Groups("user:read")
     * @ApiProperty(readableLink=false, writableLink=true)
     */
    private $champignons_refus;

    /**
     * @ORM\ManyToMany(targetEntity=Modification::class, mappedBy="acceptations")
     * @Groups("user:read")
     * @ApiProperty(readableLink=false, writableLink=true)
     */
    private $modifications_acceptations;

    /**
     * @ORM\ManyToMany(targetEntity=Modification::class, mappedBy="refus")
     * @Groups("user:read")
     * @ApiProperty(readableLink=false, writableLink=true)
     */
    private $modifications_refus;

    /**
     * @ORM\ManyToMany(targetEntity=Proposition::class, mappedBy="acceptations")
     * @Groups("user:read")
     * @ApiProperty(readableLink=false, writableLink=true)
     */
    private $propositions_acceptations;

    /**
     * @ORM\ManyToMany(targetEntity=Proposition::class, mappedBy="refus")
     * @Groups("user:read")
     * @ApiProperty(readableLink=false, writableLink=true)
     */
    private $propositions_refus;

    /**
     * @ORM\OneToOne(targetEntity=MediaObject::class, cascade={"persist", "remove"})
     * @Groups({"user:read", "user:write"})
     * @ApiProperty(readableLink=true, writableLink=false)
     */
    private $avatar;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"user:read", "user:write"})
     */
    private $banned;

    /**
     * @ORM\OneToMany(targetEntity=Modification::class, mappedBy="author")
     * @Groups({"user:read"})
     * @ApiSubresource
     * @ApiProperty(readableLink=true, writableLink=true)
     * @MaxDepth(1)
     */
    private $modifications;

    /**
     * @ORM\OneToMany(targetEntity=Champignon::class, mappedBy="author")
     * @Groups({"user:read"})
     * @ApiSubresource
     * @ApiProperty(readableLink=false, writableLink=true)
     * @MaxDepth(1)
     */
    private $champignons;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read", "user:write"})
     */
    private $email;

    /** 
     * @ORM\OneToMany(targetEntity=Proposition::class, mappedBy="user")
     * @Groups({"user:read"})
     * @ApiSubresource
     * @ApiProperty(readableLink=false, writableLink=true)
     * @MaxDepth(1)
     */
    private $propositions;


    public function __construct()
    {
        $this->champignons_acceptations = new ArrayCollection();
        $this->champignons_refus = new ArrayCollection();
        $this->modifications_acceptations = new ArrayCollection();
        $this->modifications_refus = new ArrayCollection();
        $this->propositions_acceptations = new ArrayCollection();
        $this->propositions_refus = new ArrayCollection();
        $this->modifications = new ArrayCollection();
        $this->champignons = new ArrayCollection();
        $this->propositions = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Champignon[]
     */
    public function getChampignonsAcceptations(): Collection
    {
        return $this->champignons_acceptations;
    }

    public function addChampignonsAcceptation(Champignon $champignonsAcceptation): self
    {
        if (!$this->champignons_acceptations->contains($champignonsAcceptation)) {
            $this->champignons_acceptations[] = $champignonsAcceptation;
            $champignonsAcceptation->addAcceptation($this);
        }

        return $this;
    }

    public function removeChampignonsAcceptation(Champignon $champignonsAcceptation): self
    {
        if ($this->champignons_acceptations->removeElement($champignonsAcceptation)) {
            $champignonsAcceptation->removeAcceptation($this);
        }

        return $this;
    }

    /**
     * @return Collection|Champignon[]
     */
    public function getChampignonsRefus(): Collection
    {
        return $this->champignons_refus;
    }

    public function addChampignonsRefu(Champignon $champignonsRefu): self
    {
        if (!$this->champignons_refus->contains($champignonsRefu)) {
            $this->champignons_refus[] = $champignonsRefu;
            $champignonsRefu->addRefu($this);
        }

        return $this;
    }

    public function removeChampignonsRefu(Champignon $champignonsRefu): self
    {
        if ($this->champignons_refus->removeElement($champignonsRefu)) {
            $champignonsRefu->removeRefu($this);
        }

        return $this;
    }

    /**
     * @return Collection|Modification[]
     */
    public function getModificationsAcceptations(): Collection
    {
        return $this->modifications_acceptations;
    }

    public function addModificationsAcceptation(Modification $modificationsAcceptation): self
    {
        if (!$this->modifications_acceptations->contains($modificationsAcceptation)) {
            $this->modifications_acceptations[] = $modificationsAcceptation;
            $modificationsAcceptation->addAcceptation($this);
        }

        return $this;
    }

    public function removeModificationsAcceptation(Modification $modificationsAcceptation): self
    {
        if ($this->modifications_acceptations->removeElement($modificationsAcceptation)) {
            $modificationsAcceptation->removeAcceptation($this);
        }

        return $this;
    }

    /**
     * @return Collection|Modification[]
     */
    public function getModificationsRefus(): Collection
    {
        return $this->modifications_refus;
    }

    public function addModificationsRefu(Modification $modificationsRefu): self
    {
        if (!$this->modifications_refus->contains($modificationsRefu)) {
            $this->modifications_refus[] = $modificationsRefu;
            $modificationsRefu->addRefu($this);
        }

        return $this;
    }

    public function removeModificationsRefu(Modification $modificationsRefu): self
    {
        if ($this->modifications_refus->removeElement($modificationsRefu)) {
            $modificationsRefu->removeRefu($this);
        }

        return $this;
    }

    /**
     * @return Collection|Propositions[]
     */
    public function getPropositionsAcceptations(): Collection
    {
        return $this->propositions_acceptations;
    }

    public function addPropositionsAcceptation(Proposition $propositionsAcceptation): self
    {
        if (!$this->propositions_acceptations->contains($propositionsAcceptation)) {
            $this->propositions_acceptations[] = $propositionsAcceptation;
            $propositionsAcceptation->addAcceptation($this);
        }

        return $this;
    }

    public function removePropositionsAcceptation(Proposition $propositionsAcceptation): self
    {
        if ($this->propositions_acceptations->removeElement($propositionsAcceptation)) {
            $propositionsAcceptation->removeAcceptation($this);
        }

        return $this;
    }

    /**
     * @return Collection|Proposition[]
     */
    public function getPropositionsRefus(): Collection
    {
        return $this->propositions_refus;
    }

    public function addPropositionsRefu(Proposition $propositionsRefu): self
    {
        if (!$this->propositions_refus->contains($propositionsRefu)) {
            $this->propositions_refus[] = $propositionsRefu;
            $propositionsRefu->addRefu($this);
        }

        return $this;
    }

    public function removePropositionsRefu(Modification $propositionssRefu): self
    {
        if ($this->propositions_refus->removeElement($propositionssRefu)) {
            $propositionssRefu->removeRefu($this);
        }

        return $this;
    }

    public function getAvatar(): ?MediaObject
    {
        return $this->avatar;
    }

    public function setAvatar(?MediaObject $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getBanned(): ?bool
    {
        return $this->banned;
    }

    public function setBanned(bool $banned): self
    {
        $this->banned = $banned;

        return $this;
    }

    /**
     * @return Collection|Modification[]
     */
    public function getModifications(): Collection
    {
        return $this->modifications;
    }

    public function addModification(Modification $modification): self
    {
        if (!$this->modifications->contains($modification)) {
            $this->modifications[] = $modification;
            $modification->setAuthor($this);
        }

        return $this;
    }

    public function removeModification(Modification $modification): self
    {
        if ($this->modifications->removeElement($modification)) {
            // set the owning side to null (unless already changed)
            if ($modification->getAuthor() === $this) {
                $modification->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Champignon[]
     */
    public function getChampignons(): Collection
    {
        return $this->champignons;
    }

    public function addChampignon(Champignon $champignon): self
    {
        if (!$this->champignons->contains($champignon)) {
            $this->champignons[] = $champignon;
            $champignon->setAuthor($this);
        }

        return $this;
    }

    public function removeChampignon(Champignon $champignon): self
    {
        if ($this->champignons->removeElement($champignon)) {
            // set the owning side to null (unless already changed)
            if ($champignon->getAuthor() === $this) {
                $champignon->setAuthor(null);
            }
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, Proposition>
     */
    public function getPropositions(): Collection
    {
        return $this->propositions;
    }

    public function addProposition(Proposition $proposition): self
    {
        if (!$this->propositions->contains($proposition)) {
            $this->propositions[] = $proposition;
            $proposition->addAcceptation($this);
        }

        return $this;
    }

    public function removeProposition(Proposition $proposition): self
    {
        if ($this->propositions->removeElement($proposition)) {
            $proposition->removeAcceptation($this);
        }

        return $this;
    }


}

