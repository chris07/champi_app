<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ModificationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\EntityTrait\SoftDeleteTrait;
use App\Entity\EntityTrait\CreatedAtTrait;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

/**
 * @UniqueEntity(
 *      fields={"champignon", "champ", "modification"},
 *      errorPath="champignon",
 *      message="Modification already exist."),
 * @ApiResource(attributes={"pagination_items_per_page"=30},
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_ADMIN')"},
 *         "post"={"security"="is_granted('ROLE_USER')"},
 *     },
 *     itemOperations={
 *         "get"={
 *             "requirements"={"id"="\d+"},
 *             "security"="is_granted('show', object)"
 *         },
 *         "patch"={"security"="is_granted('ROLE_ADMIN')"},
 *         "delete"={"security"="is_granted('ROLE_SUPER_ADMIN')"},
 *     },
 *     normalizationContext={"groups"={"modification:read" , "enable_max_depth"=true}},
 *     denormalizationContext={"groups"={"modification:write"}}
 * )
 * @ORM\Entity(repositoryClass=ModificationRepository::class)
 * @ApiFilter(DateFilter::class, properties={"createdAt", "deletedAt"})
 * @ApiFilter(BooleanFilter::class, properties={"valide"})
 * @ApiFilter(OrderFilter::class, properties={"createdAt", "deletedAt", "userName"}, arguments={"orderParameterName"="order"})
 */
class Modification
{

    use SoftDeleteTrait;
    use CreatedAtTrait;

    const CHAMP = [
        0 => "Null",
        1 => "nom commun",
        2 => "synonymes",
        3 => "chapeau",
        4 => "lames",
        5 => "anneau",
        6 => "pied",
        7 => "odeur",
        8 => "période de cueillètte",
        9 => "biotopes",
        10 => "confusions",
        11 => "famille",
        12 => "nom scientifique",
        13 => "image",
        14 => "qualité"
    ];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("modification:read")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     * @Groups({"modification:read", "modification:write","user:read"})
     */
    private $champ;

    /**
     * @var MediaObject|null
     *
     * @ORM\OneToOne(targetEntity=MediaObject::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(iri="http://schema.org/image")
     * @Groups({"modification:read", "modification:write","user:read"})
     * @ApiProperty(readableLink=true, writableLink=false)
     */
    public $image;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"modification:read", "modification:write","user:read"})
     */
    private $modification;

    /**
     * @ORM\ManyToOne(targetEntity=Champignon::class, inversedBy="modifications")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"modification:read", "modification:write","user:read"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $champignon;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"modification:read", "modification:write"})
     */
    private $valide = false;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="modifications_acceptations"),
     * @ORM\JoinTable(name="modification_user_acceptations")
     * @Groups({"modification:read", "modification:write","user:read"})
     * @ApiProperty(readableLink=false, writableLink=true)
     */
    private $acceptations;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="modifications_refus"),
     * @ORM\JoinTable(name="modification_user_refus")
     * @Groups({"modification:read", "modification:write", "user:read"})
     * @ApiProperty(readableLink=false, writableLink=true)
     */
    private $refus;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="modifications")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"modification:read", "modification:write","user:read"})
     * @ApiProperty(readableLink=true, writableLink=false)
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=Proposition::class, mappedBy="modification")
     * @Groups({"modification:read", "modification:write","user:read"})
     */
    private $propositions;

    public function __construct()
    {
        $this->acceptations = new ArrayCollection();
        $this->refus = new ArrayCollection();
        $this->propositions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModification(): ?string
    {
        return $this->modification;
    }

    public function setModification(string $modification): self
    {
        $this->modification = $modification;

        return $this;
    }

    public function getChampignon(): ?Champignon
    {
        return $this->champignon;
    }

    public function setChampignon(?Champignon $champignon): self
    {
        $this->champignon = $champignon;

        return $this;
    }

    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    public function setImage(?MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAcceptations(): Collection
    {
        return $this->acceptations;
    }

    public function addAcceptation(User $acceptation): self
    {
        if (!$this->acceptations->contains($acceptation)) {
            $this->acceptations[] = $acceptation;
        }

        return $this;
    }

    public function removeAcceptation(User $acceptation): self
    {
        $this->acceptations->removeElement($acceptation);

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getRefus(): Collection
    {
        return $this->refus;
    }

    public function addRefu(User $refu): self
    {
        if (!$this->refus->contains($refu)) {
            $this->refus[] = $refu;
        }

        return $this;
    }

    public function removeRefu(User $refu): self
    {
        $this->refus->removeElement($refu);

        return $this;
    }

     public function getChamp(): ?string
    {
        return $this->champ;
    }

    public function setChamp(string $champ): self
    {
        $this->champ = $champ;

        return $this;
    }

    public function getFormatedChamp(): string
    {
        return self::CHAMP[$this->champ];
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection<int, Proposition>
     */
    public function getPropositions(): Collection
    {
        return $this->propositions;
    }

    public function addProposition(Proposition $proposition): self
    {
        if (!$this->propositions->contains($proposition)) {
            $this->propositions[] = $proposition;
            $proposition->setModification($this);
        }

        return $this;
    }

    public function removeProposition(Proposition $proposition): self
    {
        if ($this->propositions->removeElement($proposition)) {
            // set the owning side to null (unless already changed)
            if ($proposition->getModification() === $this) {
                $proposition->setModification(null);
            }
        }

        return $this;
    }

}
