<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ChampignonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\EntityTrait\SoftDeleteTrait;
use App\Entity\EntityTrait\CreatedAtTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use App\Filter\NotIncludeFilter;

/**
 * @UniqueEntity("nomScientifique"),
 * @ApiResource(attributes={"pagination_items_per_page"=30},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_USER')"},
 *     },
 *     itemOperations={
 *         "get"={
 *             "requirements"={"id"="\d+"},
 *         },
 *         "patch"={"security"="is_granted('ROLE_SUPER_ADMIN')"},
 *         "delete"={"security"="is_granted('ROLE_SUPER_ADMIN')"},
 *     },
 *     normalizationContext={"groups"={"champignon:read"}},
 *     denormalizationContext={"groups"={"champignon:write"}}
 * )
 * @ORM\Entity(repositoryClass=ChampignonRepository::class)
 * @ApiFilter(DateFilter::class, properties={"createdAt", "deletedAt"})
 * @ApiFilter(SearchFilter::class, properties={"nomCommun": "partial",
 *                                             "chapeau": "partial",
 *                                             "lames": "partial",
 *                                             "pied": "partial",
 *                                             "anneau": "partial"})
 * @ApiFilter(BooleanFilter::class, properties={"soumission", "valide"})
 * @ApiFilter(OrderFilter::class, properties={"createdAt", "deletedAt", "nomCommun"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(ExistsFilter::class, properties={"pied"})
 * @ApiResource(attributes={"pagination_client_enabled"=true})
 */
class Champignon
{
    use SoftDeleteTrait;
    use CreatedAtTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("champignon:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $nomCommun;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $synonymes;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $chapeau;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $lames;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $anneau;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     * @ApiFilter(NotIncludeFilter::class)
     */
    private $pied;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $odeur;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $periodeCueillette;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $biotopes;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $confusions;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $famille;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $nomScientifique;

    /**
     * @var MediaObject|null
     *
     * @ORM\OneToMany(targetEntity=MediaObject::class, cascade={"persist", "remove"}, mappedBy="champignon")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     * @ApiProperty(readableLink=true, writableLink=false)
     */
    public $images;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $valide;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $soumission;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $commentaire;

    /**
     * @ORM\OneToMany(targetEntity=Modification::class, mappedBy="champignon", orphanRemoval=true)
     * @Groups({"champignon:read", "champignon:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $modifications;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="champignons_acceptations"),
     * @ORM\JoinTable(name="champignon_user_acceptations")
     * @Groups({"champignon:read", "champignon:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $acceptations;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="champignons_refus"),
     * @ORM\JoinTable(name="champignon_user_refus")
     * @Groups({"champignon:read", "champignon:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $refus;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="champignons")
     * @Groups({"champignon:read", "champignon:write"})
     * @ApiProperty(readableLink=false, writableLink=false)
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"champignon:read", "champignon:write"})
     */
    private $quality;



    public function __construct()
    {
        $this->modifications = new ArrayCollection();
        $this->acceptations = new ArrayCollection();
        $this->refus = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomCommun(): ?string
    {
        return $this->nomCommun;
    }

    public function setNomCommun(string $nomCommun): self
    {
        $this->nomCommun = $nomCommun;

        return $this;
    }

    public function getSynonymes(): ?string
    {
        return $this->synonymes;
    }

    public function setSynonymes(?string $synonymes): self
    {
        $this->synonymes = $synonymes;

        return $this;
    }

    public function getChapeau(): ?string
    {
        return $this->chapeau;
    }

    public function setChapeau(?string $chapeau): self
    {
        $this->chapeau = $chapeau;

        return $this;
    }

    public function getLames(): ?string
    {
        return $this->lames;
    }

    public function setLames(?string $lames): self
    {
        $this->lames = $lames;

        return $this;
    }

    public function getAnneau(): ?string
    {
        return $this->anneau;
    }

    public function setAnneau(?string $anneau): self
    {
        $this->anneau = $anneau;

        return $this;
    }

    public function getPied(): ?string
    {
        return $this->pied;
    }

    public function setPied(?string $pied): self
    {
        $this->pied = $pied;

        return $this;
    }

    public function getOdeur(): ?string
    {
        return $this->odeur;
    }

    public function setOdeur(?string $odeur): self
    {
        $this->odeur = $odeur;

        return $this;
    }

    public function getPeriodeCueillette(): ?string
    {
        return $this->periodeCueillette;
    }

    public function setPeriodeCueillette(?string $periodeCueillette): self
    {
        $this->periodeCueillette = $periodeCueillette;

        return $this;
    }

    public function getBiotopes(): ?string
    {
        return $this->biotopes;
    }

    public function setBiotopes(?string $biotopes): self
    {
        $this->biotopes = $biotopes;

        return $this;
    }

    public function getConfusions(): ?string
    {
        return $this->confusions;
    }

    public function setConfusions(?string $confusions): self
    {
        $this->confusions = $confusions;

        return $this;
    }

    public function getFamille(): ?string
    {
        return $this->famille;
    }

    public function setFamille(string $famille): self
    {
        $this->famille = $famille;

        return $this;
    }

    public function getNomScientifique(): ?string
    {
        return $this->nomScientifique;
    }

    public function setNomScientifique(string $nomScientifique): self
    {
        $this->nomScientifique = $nomScientifique;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

    public function getSoumission(): ?bool
    {
        return $this->soumission;
    }

    public function setSoumission(bool $soumission): self
    {
        $this->soumission = $soumission;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * @return Collection|Modification[]
     */
    public function getModifications(): Collection
    {
        return $this->modifications;
    }

    public function addModification(Modification $modification): self
    {
        if (!$this->modifications->contains($modification)) {
            $this->modifications[] = $modification;
            $modification->setChampignon($this);
        }

        return $this;
    }

    public function removeModification(Modification $modification): self
    {
        if ($this->modifications->removeElement($modification)) {
            // set the owning side to null (unless already changed)
            if ($modification->getChampignon() === $this) {
                $modification->setChampignon(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getAcceptations(): Collection
    {
        return $this->acceptations;
    }

    public function addAcceptation(User $acceptation): self
    {
        if (!$this->acceptations->contains($acceptation)) {
            $this->acceptations[] = $acceptation;
        }

        return $this;
    }

    public function removeAcceptation(User $acceptation): self
    {
        $this->acceptations->removeElement($acceptation);

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getRefus(): Collection
    {
        return $this->refus;
    }

    public function addRefu(User $refu): self
    {
        if (!$this->refus->contains($refu)) {
            $this->refus[] = $refu;
        }

        return $this;
    }

    public function removeRefu(User $refu): self
    {
        $this->refus->removeElement($refu);

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getQuality(): ?string
    {
        return $this->quality;
    }

    public function setQuality(?string $quality): self
    {
        $this->quality = $quality;

        return $this;
    }

}
