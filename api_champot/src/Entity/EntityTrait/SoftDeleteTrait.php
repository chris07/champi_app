<?php

namespace App\Entity\EntityTrait;


use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

trait SoftDeleteTrait
{
    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     * @Groups({"user:read", "user:write","champignon:read","champignon:write","modification:read","modification:write"})
     */
    private ?DateTimeInterface $deletedAt = null;

    public function getDeletedAt(): ?DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTimeInterface $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }
}
