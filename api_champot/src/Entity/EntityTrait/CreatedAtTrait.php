<?php

namespace App\Entity\EntityTrait;


use ApiPlatform\Core\Annotation\ApiResource;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

trait CreatedAtTrait
{
    /**
     * Date de création
     * @ORM\Column(name="createdAt",type="datetime", nullable=true)
     * @Groups({"user:read", "user:write","champignon:read","champignon:write","modification:read","modification:write"})
     */
    private ?DateTimeInterface $createdAt;

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;

    }

}