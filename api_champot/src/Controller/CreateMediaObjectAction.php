<?php
namespace App\Controller;

use App\Entity\MediaObject;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class CreateMediaObjectAction extends AbstractController
{

    public function __invoke(Request $request, ValidatorInterface $validator): MediaObject
    {

        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('Une image est nécessaire');
        }

        $violations = $validator->validate(
            $uploadedFile,
            new File([
                'maxSize' => '5M',
                'mimeTypes' => [
                    'image/*'
                ]
            ])
        );

        if ($violations->count() > 0) {
            
            throw new BadRequestHttpException('le fichier doit être une image!');
        }
        else{

            $mediaObject = new MediaObject();
            $mediaObject->file = $uploadedFile;

            return $mediaObject;
        }
    }
}