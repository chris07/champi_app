<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LogoutAction
 */
final class LogoutAction extends AbstractController
{
    /**
     * 
     */
    public function __invoke()
    {
    	$response = new Response();

        $response->send();

        return $response;
    }
}