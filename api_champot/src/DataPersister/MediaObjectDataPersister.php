<?php

// src/DataPersister

namespace App\DataPersister;

use App\Entity\Modification;
use App\Entity\MediaObject;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Exception\MaximumEntityException;
use Symfony\Component\Security\Core\Security;

/**
 *
 */
class MediaObjectDataPersister implements ContextAwareDataPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $_entityManager;

    /**
     * @param Request
     */
    private $_request;

    /**
     * @param Security
     */
    private $_security;

    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $request,
        Security $security
    ) {
        $this->_entityManager = $entityManager;
        $this->_request = $request->getCurrentRequest();
        $this->_security = $security;
    }


    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof MediaObject;
    }

    /**
     * @param MediaObject $data
     */
    public function persist($data, array $context = []): void
    {
        $entity = $this->_request->request->get('entity');
        $author = $this->_security->getUser();
        $count =  $this->_entityManager->getRepository(Modification::class)->countNumberOfImages($author)["numberOfImages"];

        if ($entity !== "user") {

            if ($count < 5) {
                $this->_entityManager->persist($data);
                $this->_entityManager->flush();
                $modification = new Modification();
                // set author and mediaobject
                $modification->setAuthor($author);
                $modification->setChamp(13);
                $modification->setImage($data);
                $modification->setCreatedAt(new \DateTime());
                $this->_entityManager->persist($modification);
                $this->_entityManager->flush();
            }
            else {
                throw new MaximumEntityException(sprintf('The maximum number of Modifications is done.'));
            }
        }  
        else {

            $old = $author->getAvatar();
            if($old !== null){
                $this->_entityManager->remove($old);
            }
            $author->setAvatar($data);
            $this->_entityManager->persist($author);
            $this->_entityManager->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}
