<?php

// src/DataPersister

namespace App\DataPersister;

use App\Entity\Modification;
use App\Entity\Champignon;
use App\Repository\ChampignonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\Security\Core\Security;

/**
 *
 */
class ModificationDataPersister implements ContextAwareDataPersisterInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $_entityManager;

    /**
     * @param Request
     */
    private $_request;

    /**
     * @param Repository
     */
    private $_repositoryChamp;

    /**
     * @param Security
     */
    private $_security;

    public function __construct(
        EntityManagerInterface $entityManager,
        ChampignonRepository $repositoryChamp,
        RequestStack $request,
        Security $security
    ) {
        $this->_entityManager = $entityManager;
        $this->_repository = $repositoryChamp;
        $this->_request = $request->getCurrentRequest();
        $this->_security = $security;
    }


    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Modification;
    }

    /**
     * @param Modification $data
     */
    public function persist($data, array $context = [])
    {

        // Set the author if it's a new modification
                if ($this->_request->getMethod() === 'POST') {

                    $data->setAuthor($this->_security->getUser());
                }

        // Set the Champignon if valide = true
        if ($this->_request->getMethod() !== 'PATCH' && $data->getValide()) {

            $champignonId = $data->getChampignon();
            $champignon = $this->_repository->find($champignonId);
            $champ = $data->getChamp();
            $modif = $data->getModification();
            
            // Modify champignon

            //switch case sur champ et set le champ

            // Save champignon
            $this->_entityManager->persist($champignon);
            $this->_entityManager->flush();
        }

        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}