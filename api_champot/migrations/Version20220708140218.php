<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220708140218 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE proposition (id INT AUTO_INCREMENT NOT NULL, modification_id INT DEFAULT NULL, champignon_id INT NOT NULL, INDEX IDX_C7CDC3534A605127 (modification_id), INDEX IDX_C7CDC3533D233456 (champignon_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proposition ADD CONSTRAINT FK_C7CDC3534A605127 FOREIGN KEY (modification_id) REFERENCES modification (id)');
        $this->addSql('ALTER TABLE proposition ADD CONSTRAINT FK_C7CDC3533D233456 FOREIGN KEY (champignon_id) REFERENCES champignon (id)');
        $this->addSql('ALTER TABLE modification DROP propositions');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE proposition');
        $this->addSql('ALTER TABLE modification ADD propositions LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\'');
    }
}
