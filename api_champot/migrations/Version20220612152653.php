<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220612152653 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX nom_scientifique ON champignon');
        $this->addSql('ALTER TABLE media_object DROP INDEX IDX_14D431324A605127, ADD UNIQUE INDEX UNIQ_14D431324A605127 (modification_id)');
        $this->addSql('DROP INDEX file_path ON media_object');
        $this->addSql('DROP INDEX champignon_id ON modification');
        $this->addSql('ALTER TABLE modification ADD image_id INT DEFAULT NULL, CHANGE modification modification LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE modification ADD CONSTRAINT FK_EF6425D23DA5256D FOREIGN KEY (image_id) REFERENCES media_object (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EF6425D23DA5256D ON modification (image_id)');
        $this->addSql('DROP INDEX username ON user');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX nom_scientifique ON champignon (nom_scientifique)');
        $this->addSql('ALTER TABLE media_object DROP INDEX UNIQ_14D431324A605127, ADD INDEX IDX_14D431324A605127 (modification_id)');
        $this->addSql('CREATE UNIQUE INDEX file_path ON media_object (file_path, champignon_id)');
        $this->addSql('ALTER TABLE modification DROP FOREIGN KEY FK_EF6425D23DA5256D');
        $this->addSql('DROP INDEX UNIQ_EF6425D23DA5256D ON modification');
        $this->addSql('ALTER TABLE modification DROP image_id, CHANGE modification modification TEXT NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX champignon_id ON modification (champignon_id, champ, author_id)');
        $this->addSql('CREATE UNIQUE INDEX username ON user (username)');
    }
}
