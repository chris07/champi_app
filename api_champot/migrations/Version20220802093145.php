<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220802093145 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE proposition_user_acceptations (proposition_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_834FD321DB96F9E (proposition_id), INDEX IDX_834FD321A76ED395 (user_id), PRIMARY KEY(proposition_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proposition_user_refus (proposition_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_5460F6B2DB96F9E (proposition_id), INDEX IDX_5460F6B2A76ED395 (user_id), PRIMARY KEY(proposition_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proposition_user_acceptations ADD CONSTRAINT FK_834FD321DB96F9E FOREIGN KEY (proposition_id) REFERENCES proposition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proposition_user_acceptations ADD CONSTRAINT FK_834FD321A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proposition_user_refus ADD CONSTRAINT FK_5460F6B2DB96F9E FOREIGN KEY (proposition_id) REFERENCES proposition (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proposition_user_refus ADD CONSTRAINT FK_5460F6B2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE proposition_user_acceptations');
        $this->addSql('DROP TABLE proposition_user_refus');
    }
}
