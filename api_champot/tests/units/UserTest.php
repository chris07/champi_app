<?php
declare(strict_types=1);

namespace App\Tests\Units;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Champignon;
use App\Entity\MediaObject;
use App\Entity\Modification;
use App\Entity\User;


class UserTest extends ApiTestCase
{
    private User $user;

    protected function setUp():void
    {
        parent::setUp();

        $this->user = new User();
    }

    public function testGetEmail():void
    {
        $value = "test@test.fr";

        $response = $this->user->setEmail($value);
        $getEmail = $this->user->getEmail();

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $getEmail);
    }

    public function testUsername():void
    {
        $value = "test";

        $response = $this->user->setUsername($value);
        $getUsername = $this->user->getUsername();

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $getUsername);
    }

    public function testGetRoles():void
    {
        $value = ["ROLE_ADMIN"];
        $response = $this->user->setRoles($value);

        self::assertInstanceOf(User::class, $response);
        self::assertContains("ROLE_USER", $this->user->getRoles());
        self::assertContains("ROLE_ADMIN", $this->user->getRoles());
    }

    public function testGetPassword():void
    {
        $value = "password";
        $response = $this->user->setPassword($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value, $this->user->getPassword());
    }

    public function testGetBanned():void
    {
        $value = true;
        $response = $this->user->setBanned($value);

        self::assertInstanceOf(User::class, $response);
        self::assertTrue($this->user->getBanned());

        $value = false;
        $response = $this->user->setBanned($value);

        self::assertFalse($this->user->getBanned());
    }

    public function testGetChampignons():void
    {
        $value = new Champignon();
        $response = $this->user->addChampignon($value);

        self::assertInstanceOf(User::class, $response);
        self::assertCount(1 , $this->user->getChampignons());
        self::assertTrue($this->user->getChampignons()->contains($value));

        $response = $this->user->removeChampignon($value);

        self::assertInstanceOf(User::class, $response);
        self::assertCount(0 , $this->user->getChampignons());
        self::assertFalse($this->user->getChampignons()->contains($value));
    }

    public function testGetModifications():void
    {
        $value = new Modification();
        $response = $this->user->addModification($value);

        self::assertInstanceOf(User::class, $response);
        self::assertCount(1 , $this->user->getModifications());
        self::assertTrue($this->user->getModifications()->contains($value));

        $response = $this->user->removeModification($value);

        self::assertInstanceOf(User::class, $response);
        self::assertCount(0 , $this->user->getModifications());
        self::assertFalse($this->user->getModifications()->contains($value));
    }

    public function testGetAvatar():void
    {
        $value = new MediaObject();
        $response = $this->user->setAvatar($value);

        self::assertInstanceOf(User::class, $response);
        self::assertEquals($value,$this->user->getAvatar());

        $value = null;
        $response = $this->user->setAvatar($value);

        self::assertEquals($value, $this->user->getAvatar());
    }
}