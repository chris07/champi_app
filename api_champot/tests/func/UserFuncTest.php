<?php
declare(strict_types=1);

namespace App\Tests\Func;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Champignon;
use App\Entity\MediaObject;
use App\Entity\Modification;
use App\Entity\User;

class UserTest extends ApiTestCase
{

    public function testGetCollection(): void
    {
        $response = static::createClient()->request('GET', 'https://127.0.0.1:8001/api/users');

        $this->assertResponseStatusCodeSame(401);
      
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }
}