import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthService from '../services/auth.service';
import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {

    const jwt = JSON.parse(localStorage.getItem('jwt'));

    if(jwt !== null){
      const tokens = jwt.split(".");
      const token = JSON.parse(window.atob(tokens[1]));
      const exp = token.exp;
      let now = Date.now().toString();
      now = parseInt(now.substring(0, now.length-3));
      const diff = exp - now;

      if(diff < 300){
        AuthService.refreshJWT();
      }
    }
    next()
  })

  return Router
}
