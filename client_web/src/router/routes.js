
const routes = [
  {
    path: '/',
    name: '',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { 
        path: '',
        name:'home',
        component: () => import('src/pages/Home.vue') 
      },
      { 
        path: 'signin', 
        name: 'signIn',
        component: () => import('pages/SignIn.vue') 
      },
      { 
        path: 'login', 
        name: 'login',
        component: () => import('pages/Login.vue') 
      },
      { 
        path: 'profile', 
        name: 'profile',
        component: () => import('pages/Profile.vue') 
      },
      {
        path: 'champignons',
        name: 'champignons',
        component: () => import('pages/Champignons.vue'),
        children: [
          { 
            path: ':id',
            name:'champignon-detail',
            component: () => import('pages/Champignon.vue'),
            props: true,
          },
        ]
      },
    ]
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { 
        path: 'board',
        name:'adminBoard',
        component: () => import('pages/AdminBoard.vue') 
      },
      { 
        path: 'propositions',
        name:'adminPropositions',
        component: () => import('pages/PropositionsAdmin.vue') 
      },
    ]
  },
  {
    path: '/user',
    name: 'user',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { 
        path: 'board',
        name:'userBoard',
        component: () => import('pages/UserBoard.vue') 
      },
      { 
        path: 'upload',
        name:'upload',
        component: () => import('pages/Upload.vue') 
      },
    ]
  },
  {
    path: '/superadmin',
    name: 'superAdmin',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { 
        path: 'board',
        name:'superAdminBoard',
        component: () => import('pages/SuperAdminBoard.vue') 
      },
      { 
        path: 'users',
        name:'users',
        component: () => import('pages/Users.vue') 
      },
    ]
  },
  {
    path: '*',
    name: '404',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
