import AuthService from '../services/auth.service';

const initialState = { status: { loggedIn: false }, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    async set({ commit }){
      await AuthService.getMe().then(
        user => {
          if(user !== null){
            commit('set', user);
            return Promise.resolve(user);
          }
        },
        error => {
          commit('setError');
          return Promise.reject(error);
        }
      );
    },
    async login({ commit }, user) {
        try {
          const resp = await AuthService.login(user)
          commit('loginSuccess', resp);
          return true;
        } 
        catch (error) {
          commit('loginFailure');
          return error;
        }    
    },
    logout({ commit }) {
      AuthService.logout();
      commit('logout');
    },
    async register({ commit }, user) {

      try {
        const resp = await AuthService.register(user)
        commit('registerSuccess');
        return true;
      } 
      catch (error) {
        commit('registerFailure');
        return false;
      }
    }
  },
  mutations: {
    set(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },
    setError(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    loginSuccess(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    logout(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    registerSuccess(state) {
      state.status.loggedIn = false;
    },
    registerFailure(state) {
      state.status.loggedIn = false;
    }
  }
};
