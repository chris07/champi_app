import UserService from '../services/user.service';
import AuthService from '../services/auth.service';

const initialState = { user: null, infos: null };

export const user = {
  namespaced: true,
  state: initialState,
  actions: {
    async save({ commit }, user) {

      try {
        const resp = await UserService.save(user.id, user)
        commit('saveSuccess', resp);
        return true;
      } 
      catch (error) {
        commit('saveFailure');
        return false;
      }
    },
    async updatePassword({ commit }, user) {

      try {
        const resp = await UserService.updatePassword(user.id, user)
        commit('saveSuccess', resp);
        return true;
      } 
      catch (error) {
        commit('saveFailure');
        return false;
      }
    },

    async getMyInfos({ commit }, user) {
      try {
        const resp = await UserService.getUserBoard(user)
        commit('infosSuccess', resp);
        return true;
      } 
      catch (error) {
        commit('infosFailure');
        return false;
      }    
  },
  },
  mutations: {
    saveSuccess(state, resp) {
      state.user = resp.data.id;
    },
    saveFailure(state) {
      state.user = null;
    },
    infosSuccess(state, resp) {
      state.infos = resp.data;
    },
    infosFailure(state) {
      state.infos = null;
    }
  }
};
