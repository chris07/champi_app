import axios from 'axios';
import {authHeader} from './auth-header';
import {apiUrl} from 'src/config/api';

const API_URL = apiUrl();

class AuthService {

  //Connecte un utilisateur
  async login(user) {
    await axios
      .post(API_URL + 'login_check',
        {
          username: user.userName,
          password: user.password
        }
      ).then((response) => {
        localStorage.setItem('jwt', JSON.stringify(response.data.token));
        localStorage.setItem('refresh', JSON.stringify(response.data.refresh_token));
      })
    var me = this.getMe()
      .then(response => {
        if (response === null){
          throw new Error("Erreur serveur");
        }
        else{
          return response;
        }
      });
    return me;
  }

  //Déconnecte un utilisateur
  logout() {
      localStorage.removeItem('jwt');
      localStorage.removeItem('refresh');
  }

  //Crée un utilisateur
  register(user) {
    return axios.post(API_URL + 'users', {
      username: user.userName,
      email: user.email,
      password: user.password,
      banned: false,
    });
  }

   //retourne l'utilisateur connecté
  getMe(){
    return  axios
      .get(API_URL + "users/" + "me", { headers: authHeader() })
      .then(response => { 
        return response.data;
      },
      error => {
          (error.response && error.response.data) ||
          error.message ||
          error.toString();
          return null;
      }
    );
  }

  // rafraichi l'access token
  refreshJWT(){
    const tokenRefresh = JSON.parse(localStorage.getItem('refresh'));
    return  axios
      .post(API_URL + "token/refresh", {
        refresh_token: tokenRefresh,
      })
      .then(response => { 
        localStorage.setItem('jwt', JSON.stringify(response.data.token));
        localStorage.setItem('refresh', JSON.stringify(response.data.refresh_token));
        return true
      },
      error => {
          (error.response && error.response.data) ||
          error.message ||
          error.toString();
          return null;
      }
    );
  }
}

export default new AuthService();
