import axios from 'axios';
import {authHeader} from './auth-header';
import {apiUrl} from 'src/config/api';

const API_URL = apiUrl();

class ChampiService {

  //Retourne tous les champignons
  getAll(page){
     return axios.get(API_URL + "champignons?page=" + page + "&order[nomCommun]=asc", { headers: authHeader() });
  }

  //Retourne tous les champignons résultants d'une recherche
  getAllWithSearch(page, search){
    return axios.get(API_URL + "champignons?page=" + page + search + "&order[nomCommun]=asc", { headers: authHeader() });
 }

  //Retourne un champignon
  getOne(id) {
      return axios.get(API_URL + "champignons/" + id, { headers: authHeader() });
  }

  //Modifie un champignon
  update(id, champignon) {
   return axios.patch(API_URL + "champignons/" + id , {
      email: champignon.email,
     
    }, { headers: authHeader() });
  }

  //supprime partiellement un champignon
  softDelete(id) {
    return axios.patch(API_URL + "champignons/" + id, {
      deletedAt: Date.now(),
    }, { headers: authHeader() });
  }

  //Supprime un champignon
  delete(id) {
      return axios.delete(API_URL + "champignons/" + id, { headers: authHeader() });
  }
}

export default new ChampiService();
