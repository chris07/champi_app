import axios from 'axios';
import {authHeader, authHeaderPatch, authHeaderImagePost} from './auth-header';
import {apiUrl} from 'src/config/api';

const API_URL = apiUrl();

class UserService {

  //Retourne les infos de l'utilisateur pour le dashboard
  getSuperAdminBoard(id) {
    return axios.get(API_URL + "users/" + id,{ headers: authHeader() });
  }

  //Retourne les infos de l'utilisateur pour le dashboard
  getAdminBoard(id) {
    return axios.get(API_URL + "users/" + id, { headers: authHeader() });
  }

  //Retourne les infos de l'utilisateur pour le dashboard
  getUserBoard(id) {
    return axios.get(API_URL + "users/" + id, { headers: authHeader() });
  }

  //Retourne les champignons de l'utilisateur
  getMyChampi(id) {
    return axios.get(API_URL + "users/" + id + "/champignons", { headers: authHeader() });
  }

  //Retourne les modifications de l'utilisateur
  getMyModif(id) {
    return axios.get(API_URL + "users/" + id + "/modifications",{ headers: authHeader() });
  }

  //Retourne tous les utilisateurs
  getAll(){
     return axios.get(API_URL + "users", { headers: authHeader() });
  }

  //Retourne un utilisateur
  getOne(id) {
      return axios.get(API_URL + "users/" + id, { headers: authHeader() });
  }

  //Modifie les roles d'un utilisateur
  updateRoles(id, user) {
   return axios.patch(API_URL + "users/" + id , {
        roles: user
    }, { headers: authHeaderPatch() });
  }

  //Modifie le profile d'un utilisateur
  save(id, user) {
   return axios.patch(API_URL + "users/" + id , {
      email: user.email,
    }, { headers: authHeaderPatch() });
  }

  //Modifie le mot de passe et l'utilisateur
  updatePassword(id, user) {
   return axios.patch(API_URL + "users/" + id , {
      password: user.password,
    },{ headers: authHeaderPatch() });
  }

  //supprime partiellement un utilisateur
  softDelete(id) {
    return axios.patch(API_URL + "users/" + id, {
      deletedAt: Date.now(),
    }, { headers: authHeader() });
  }

  //Supprime un utilisateur
  delete(id) {
      return axios.delete(API_URL + "users/" + id, { headers: authHeader() });
  }
}

export default new UserService();
