export function authHeader() {
  let token = JSON.parse(localStorage.getItem('jwt'));
  if (token) {
    return { Authorization: 'Bearer ' + token };
  } else {
    return {};
  }
}

export function authHeaderPatch() {
  let token = JSON.parse(localStorage.getItem('jwt'));
  if (token) {
    return { Authorization: 'Bearer ' + token , 'Content-Type': 'application/merge-patch+json'};
  } else {
    return {};
  }
}

export function authHeaderImagePost() {
  let token = JSON.parse(localStorage.getItem('jwt'));
  if (token) {
    return { Authorization: 'Bearer ' + token , 'Content-Type': 'multipart/form-data'};
  } else {
    return {};
  }
}
