const fs = require('fs');
const request = require('request');
const fichier = require('./db_champot.json');
//console.log(fichier);
let imgData = [];


function createImgFolder(){
    return new Promise((resolve)=>{
        fichier.forEach((champot)=>{
            champot['Images'].forEach((img,index)=>{
                img = img.replace(/é/g, 'e');
                img = img.replace(/è/g, 'e');
                img = img.replace(/ê/g, 'e');
                img = img.replace(/ï/g, 'i');
                img = img.replace(/â/g, 'a');
                img = img.replace(/à/g, 'a');

                if (index === champot['Images'].length-1) {
                    imgData.push(champot['Images']);
                }
            })
        })
        resolve(true);
        //console.log(imgData);
    })
}


function scrapAndTransfert() {
    
    imgData.forEach((el) =>{
        //console.log(el);
        let name = el[0].replace('http://mycorance.free.fr/valchamp/valimage/','');
        name = name.replace('1','');
        name = name.replace('2','');
        name = name.replace('3','');
        name = name.replace('4','');
        name = name.replace('.png', '');
        name = name.replace('.jpg', '');
        name = name.replace('.jpeg', '');
        //console.log(name);
        downloadPictures(el,name);
    });
}


function downloadPictures(champignon, name) {
    champignon.forEach((element,index) => {
        let type = element.substring(element.lastIndexOf('.') + 1);
        download(element, './imagesdl/'+name+'_'+index+'.'+type);        
    });
}


function download(url, dest) {
    const file = fs.createWriteStream(dest);
    const sendReq = request.get(url);
    sendReq.on('response', (response) => {
        if (response.statusCode !== 200) {
            console.log('Response status was ' + response.statusCode + dest);
        }
    });
    sendReq.on('error', (err) => {
        fs.unlink(dest);
        console.log(err)
    });
    sendReq.pipe(file);
    file.on('finish', () => {
        file.close();
    });
    file.on('error', (err) => {
        fs.unlink(dest);
        console.log(err)
    });
};

createImgFolder();
scrapAndTransfert();

