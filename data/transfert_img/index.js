const fs = require('fs')
let fichier = require('./db_champot.json')

let newFichier = [];

function modify(fichier){
 	return new Promise((resolve)=>{
 		fichier.forEach((champot)=>{
			champot['Images'].forEach((img,index)=>{
				let champignon = champot;
				let ext = img.substring(img.lastIndexOf('.') + 1);
				let nomBase = champot['Nom Commun'].replace(/ /g, '_');
				let oldUrl = './images/' + nomBase + '_' + index + '.' + ext;
				let nom = nomBase.replace(/à/g, 'a');
				nom = nom.replace(/é/g, 'e');
				nom = nom.replace(/è/g, 'e');
				nom = nom.replace(/ê/g, 'e');
				nom = nom.replace(/ï/g, 'i');
				champignon['Images'][index] = '/images/' + nom + '_' + index + '.' + ext;

				if (index === champot['Images'].length-1) {
					newFichier.push(champignon);
				}
			})
		})
		resolve(true);
	})
}

modify(fichier).then((response)=>{
	let data = JSON.stringify(newFichier);
	data = data.replace(/undefined/g, 'Commentaire')
	data = data.replace(/Lamelles/g, 'Lames')
	data = data.replace(/Nom Commun/g, 'Nom_Commun')
	data = data.replace(/Nom scientifique/g, 'Nom_Scientifique')
	data = data.replace(/Période de cueillette/g, 'Periode_de_cueillette')
	
	fs.writeFile("champot.json", data, 'utf8', function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The data has been scraped and saved successfully! View it at './champot.json'");
    });
    
})


