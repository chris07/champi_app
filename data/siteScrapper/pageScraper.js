const scraperObject = {
    url: 'http://mycorance.free.fr/valchamp/',
    async scraper(browser){
        let page = await browser.newPage();
        console.log(`Navigating to ${this.url}...`);
        await page.goto(this.url + 'alfa.htm');
        await page.waitForSelector('body');
        let urls = await page.$$eval(' #listcom1 > a', links => {
    		links = links.filter(link => link.href)
            links = links.map(el => el.href)
            return links;
        });
        
        let pagePromise = (link) => new Promise(async(resolve, reject) => {
            let dataObj = {};
            let newPage = await browser.newPage();
            await newPage.goto(link);
            
            dataObj["Nom Commun"] = await newPage.$eval('h1', text =>  text.textContent.substring(text.textContent.indexOf(':')+1, text.textContent.length).trim());
            dataObj["Images"] = await newPage.$$eval('#photo > img', links => {
            	links = links.filter(link => link.src)
            	links = links.map(el => el.src)
            	return links;
            });
            try {
              await newPage.waitForSelector('#photo > p', { timeout: 2000 })
              dataObj["Synonymes"] = await newPage.$eval('#photo > p', text =>  text.textContent.substring(text.textContent.indexOf(':')+1, text.textContent.length).replace('latin',' latin').trim());
            } catch (error) {
              dataObj["Synonymes"] = null;
            }
            let tempInfos = await newPage.$$eval(' #cadre > #descr > p > strong', text =>  {
            	let txts = text.filter(res => res.textContent)
            	txts = text.map(el => el.textContent.substring(0,el.textContent.length -1).trim())
            	return txts;
            });
            let infos = await newPage.$$eval(' #cadre > #descr > p', text =>  {
            	let txts = text.filter(res => res.textContent)
            	txts = text.map(el => el.textContent.replace(/[\n\r\t]/g,'').substring(el.textContent.indexOf(':')+1, el.length).trim())
            	return txts;
            });
            for (let i=0; i<infos.length; i++ ){
	            dataObj[tempInfos[i]] = infos[i];
            }
            
            resolve(dataObj);
            await newPage.close();
        });

        let scrapedData=[];
        let item =1;
        for(link in urls){
            let currentPageData = await pagePromise(urls[link]);
            scrapedData.push(currentPageData);
            item++;
        }

        return scrapedData;
    }
}

module.exports = scraperObject;